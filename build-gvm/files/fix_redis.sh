#! /bin/bash
cp /etc/redis/redis.conf /etc/redis/redis.orig
cp /opt/gvm/src/openvas-20.08/config/redis-openvas.conf /etc/redis/
chown redis:redis /etc/redis/redis-openvas.conf
echo "db_address = /run/redis-openvas/redis.sock" > /opt/gvm/etc/openvas/openvas.conf
systemctl enable redis-server@openvas.service
systemctl start redis-server@openvas.service
sysctl -w net.core.somaxconn=1024
sysctl vm.overcommit_memory=1
echo "net.core.somaxconn=1024"  >> /etc/sysctl.conf
echo "vm.overcommit_memory=1" >> /etc/sysctl.conf
